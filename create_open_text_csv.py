#!/usr/bin/env python3


import argparse
import csv

init_value = -1


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("students_csv")
    parser.add_argument("points_csv")
    parser.add_argument("first_open_task", type=int)
    parser.add_argument("last_open_task", type=int)
    args = parser.parse_args()
    students_file = args.students_csv
    points_file = args.points_csv
    task_range = range(args.first_open_task, (args.last_open_task + 1))

    with open(students_file, "r") as sf:
        student_list = list(csv.reader(sf))
    # Skip header row
    legi = [row[3] for row in student_list[1:]]
    legi.sort()

    header = ["open_legi"] + [f"open_task_{i}" for i in task_range]
    data_init = [init_value for i in task_range]
    data = [([lg] + data_init) for lg in legi]

    with open(points_file, "w") as pf:
        cw = csv.writer(pf)
        cw.writerow(header)
        cw.writerows(data)


if __name__ == "__main__":
    main()
