#!/usr/bin/env python3

import argparse
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


def calc_passed(column):
    passed = column[column >= 4].count()
    total = column.count()
    return passed*100./total

def load_summary(summary_csv):
    df = pd.read_csv(summary_csv,sep=",")
    #df["grade_no_bonus_rounded"]=df["grade_no_bonus"].apply(round_quarter)
    #df["final_grade_rounded"]=df["final_grade"].apply(round_quarter)
    return df

def plot_grade(df, name, bin_edges,prefix):
    column = df[name]
    fig, ax = plt.subplots()
    ax.hist(column,bins=bin_edges, edgecolor="black",)
    ax.set(xlabel="grade", ylabel="counts", title=name)
    textstr='\n'.join((
        r'N= %i ' % (column.count(), ),
        r'mean=%.2f' % (column.mean(), ),
        r'std=%.2f' % (column.std()),
        r'passed=%0.2f%%' %(calc_passed(column))
        ))
    props = dict(boxstyle='round', facecolor='wheat', alpha=0.8)
    ax.text(0.05, 0.85, textstr, transform=ax.transAxes, fontsize=11,
        verticalalignment='top', bbox=props)
    ax.grid()
    fig.savefig("Plots/"+prefix+"_"+name+".pdf",dpi=300)

def plot_question(df, name, bin_edges, maxP):
    column = df[name]
    fig, ax = plt.subplots()
    ax.hist(column,bins=bin_edges, edgecolor="black",)
    ax.set(xlabel="points", ylabel="counts", title=name)
    textstr='\n'.join((
        r'N= %i ' % (column[column > 0].count()),
        r'mean=%.2f' % (column.mean(), ),
        r'std=%.2f' % (column.std()),
        r'max= %i ' % (maxP),
        ))
    props = dict(boxstyle='round', facecolor='wheat', alpha=0.8)
    ax.text(0.05, 0.85, textstr, transform=ax.transAxes, fontsize=11,
        verticalalignment='top', bbox=props)
    ax.grid()
    fig.savefig("Plots/"+name+".pdf",dpi=300)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("summary_csv")
    parser.add_argument("prefix")
    args = parser.parse_args()
    summary_csv = args.summary_csv
    prefix =args.prefix

    summary = load_summary(summary_csv)
    plot_grade(summary, "final_grade_rounded", np.arange(2.875,6.25,0.25),prefix)
    plot_grade(summary, "grade_no_bonus_rounded",np.arange(2.875,6.25,0.25),prefix)

    plot_question(summary, "amc_total",  np.arange(9.5,36.5,1),35)
    plot_question(summary, "total_points",  np.arange(24.5,95.6,1), 95)
    maxpoints = [6,7,5,9,4,6,6,7,6,4]
    #for i,t in enumerate(np.arange(20,30,1)):
    #    plot_question(summary, r"open_task_%i" %t, np.arange(-0.125, maxpoints[i]+0.25, 0.25), maxpoints[i])



if __name__ == "__main__":
    main()
