#!/usr/bin/env python3


import argparse
import csv
import json
import math

import numpy as np
import pandas as pd


grade_key = "Note"
gradetrend_key = "Notentendenz_+/-"
number_key = "Nummer"


class Error(Exception):
    """Base class for exceptions in this module."""
    pass


class MissingEntry(Error):
    def __init__(self, file_name, data_frame):
        self.file_name = file_name
        self.df = data_frame

    def __str__(self):
        return f"{self.file_name} is missing entries in the following rows:\n\n{self.df}"


class ValidationMismatch(Error):
    def __init__(self, file_a, file_b, data_frame_diff):
        self.file_a = file_a
        self.file_b = file_b
        self.df_diff = data_frame_diff

    def __str__(self):
        return f"{self.file_a} and {self.file_b} differ:\n\n{self.df_diff}"


def round_quarter(x):
    return math.floor(x*4)/4


def load_exercise(exercise_csv):
    return pd.read_csv(exercise_csv, sep=",")


def load_amc(amc_csv):
    return pd.read_csv(amc_csv, sep=",", quotechar='"', quoting=csv.QUOTE_ALL)


def load_open_text(open_text_csv):
    df = pd.read_csv(open_text_csv, sep=",")
    # Check for missing entries (negative from template)
    # Select all rows that contain negative values, ignore first (legi) column
    rows = (df.iloc[:, 1:] < 0).any(axis="columns")
    if rows.any():
        raise MissingEntry(open_text_csv, df[rows])
    return df


def load_open_text_validated(open_text_csv_a, open_test_csv_b):
    dfa = load_open_text(open_text_csv_a)
    dfb = load_open_text(open_test_csv_b)
    diff = dfa.compare(dfb)
    if not diff.empty:
        raise ValidationMismatch(open_text_csv_a, open_test_csv_b, diff)
    return dfa


def load_grades(grades_csv):
    return pd.read_csv(grades_csv, sep=",")


def create_summary(exercise_csv, amc_csv, open_text_csv_a, open_text_csv_b):
    # Load data into data frames
    exercise = load_exercise(exercise_csv)
    amc = load_amc(amc_csv)
    open_text = load_open_text_validated(open_text_csv_a, open_text_csv_b)

    # Prepare data frames
    exercise.rename(columns={"Email address": "ex_email"}, inplace=True)
    exercise["ex_total"] = exercise.eq("Yes").sum(axis=1)
    amc.rename(columns={"A:legi": "amc_legi", "E-Mail": "amc_email", "Mark": "amc_total"}, inplace=True)
    open_text["open_total"] = open_text.filter(like="open_task_").sum(axis="columns")

    # Merge data frames
    # We'll use the open_text data frame as a starting point for the summary data frame
    summary = open_text.merge(amc[["amc_legi", "amc_email", "amc_total"]], how="left", left_on="open_legi",
                              right_on="amc_legi", validate="one_to_one", suffixes=(False, False))
    # We don't have the legi in the exercise data, only e-mail, but there's no e-mail in the open_text,
    # so we use the one from the AMC
    summary = summary.merge(exercise[["ex_email", "ex_total"]], how="left", left_on="amc_email", right_on="ex_email",
                            validate="one_to_one", suffixes=(False, False))

    # Check for missing entries
    # Select all rows containing NaN
    rows = summary.isna().any(axis="columns")
    if rows.any():
        raise MissingEntry("Summary", summary[rows])

    # Add grand total
    summary["total_points"] = summary[["open_total", "amc_total"]].sum(axis="columns")

    return summary


def add_exercise_bonus(summary, bonus, threshold):
    summary["ex_bonus"] = (summary["ex_total"] >= threshold) * bonus


def grade_scale(points, points_grade_4, points_grade_6):
    if points >= points_grade_6:
        return 6.0
    elif points >= points_grade_4:
        return 4.0 + 2 * (points - points_grade_4) / (points_grade_6 - points_grade_4)
    else:
        return 1.0 + 3 * points / points_grade_4


def calculate_grade(summary, points_grade_4, points_grade_6):
    summary["grade_no_bonus"] = summary["total_points"].apply(grade_scale, args=(points_grade_4, points_grade_6))
    summary["final_grade"] = summary[["grade_no_bonus", "ex_bonus"]].sum(axis="columns").clip(None, 6)
    summary["grade_no_bonus_rounded"]=summary["grade_no_bonus"].apply(round_quarter)
    summary["final_grade_rounded"]=summary["final_grade"].apply(round_quarter)

    #Grade tendency: #one quartile "-", two quartiles "",  one quartile "+"
    conditions = [
        (summary["final_grade"]-summary["final_grade_rounded"] - 0.125 > 0.0625) & (summary["final_grade_rounded"] < 6.0), #exclude 6+
        (summary["final_grade"]-summary["final_grade_rounded"] - 0.125 < -0.0625) & (summary["final_grade_rounded"] > 1.0), #exlcude 1-
        ]
    values = ["+", "-"]
    summary["tendency"] = np.select(conditions, values, default="")


def make_grade_table(points_min, points_max, points_grade_4, points_grade_6):
    points = np.arange(points_min,points_max+0.25,0.25)
    table = pd.DataFrame({"points": points})
    table["grade_no_rounded"]= table["points"].apply(grade_scale, args=(points_grade_4, points_grade_6))
    table["grade"] = table["grade_no_rounded"].apply(round_quarter)
    table.drop(columns=["grade_no_rounded"], inplace=True)
    return table


def insert_grades(summary, grades_csv):
    grades = load_grades(grades_csv)
    # Check for existing grades
    # Select all rows where "Note" is not NaN
    rows = grades[grade_key].notna()
    if rows.any():
        print(
            f"WARNING: The following students have existing grades, they will be overwritten or deleted:\n\n{grades[rows]}\n")

    grades = grades.merge(summary[["open_legi", "final_grade_rounded", "tendency"]], how="left", left_on=number_key, right_on="open_legi",
                          validate="one_to_one", suffixes=(False, False))
    grades.drop(columns=[grade_key, "open_legi", gradetrend_key], inplace=True)
    grades.insert(7, gradetrend_key, grades.pop("tendency"))
    grades.insert(7, grade_key, grades.pop("final_grade_rounded"))

    # Check for missing grades
    # Select all rows where "Note" is NaN
    rows = grades[grade_key].isna()
    if rows.any():
        print(f"WARNING: The following students have no grade:\n\n{grades[rows]}\n")

    return grades


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("config_json")
    parser.add_argument("exercise_csv")
    parser.add_argument("amc_csv")
    parser.add_argument("open_text_csv_a")
    parser.add_argument("open_text_csv_b")
    parser.add_argument("grades_csv")
    args = parser.parse_args()
    exercise_csv = args.exercise_csv
    amc_csv = args.amc_csv
    open_text_csv_a = args.open_text_csv_a
    open_text_csv_b = args.open_text_csv_b
    grades_in_csv = args.grades_csv

    with open(args.config_json) as cj:
        config = json.load(cj)
    summary_out_csv = f"{config['output_prefix']}_summary.csv"
    grades_out_csv = f"{config['output_prefix']}_grades.csv"
    table_out_csv = f"{config['output_prefix']}_point_table.csv"

    summary = create_summary(exercise_csv, amc_csv, open_text_csv_a, open_text_csv_b)
    add_exercise_bonus(summary, config["exercise_bonus"], config["exercise_bonus_threshold"])
    calculate_grade(summary, config["points_grade_4"], config["points_grade_6"])
    grades = insert_grades(summary, grades_in_csv)

    summary.to_csv(summary_out_csv, sep=",", index=False)
    grades.to_csv(grades_out_csv, sep="\t", index=False)

    table = make_grade_table(config["points_min"], config["points_max"], config["points_grade_4"], config["points_grade_6"])
    table.to_csv(table_out_csv, sep=",", index=False)

if __name__ == "__main__":
    main()
